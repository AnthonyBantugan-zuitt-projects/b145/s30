// console.log("Hello World");

// #4 and #5 Solution
fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {
		const array = json.map((map) => map.title);
		console.log(array);
	});

// #6 solution
fetch("https://jsonplaceholder.typicode.com/todos/10")
	.then((response) => response.json())
	.then((json) => {
		console.log(json);
		console.log(
			`The item "${json.title}" on the list has a status of ${json.completed}`
		);
	});

// #7 Solution
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json",
	},
	body: JSON.stringify({
		title: "New To Do",
		body: "I am a new to do",
		userId: 1,
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// #8 and #9 Solution
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json",
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated ToDo ist",
		description: "to update my todo list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1,
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// #10 and #11 Solution
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json",
	},
	body: JSON.stringify({
		dateCompleted: "01/26/22",
		status: "Complete",
	}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// #12 Solution
fetch("https://jsonplaceholder.typicode.com/todos/12", {
	method: "DELETE",
});
